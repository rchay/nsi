from gpiozero import LED
from time import sleep

leds = [LED(17), LED(27), LED(22), LED(10)]

for i in range(5):
    for led in leds:
        led.on()
        sleep(0.2)
    for led in leds:
        led.off()
    sleep(1)
