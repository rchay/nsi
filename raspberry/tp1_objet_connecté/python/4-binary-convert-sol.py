def display(decimal):
    binaire = binary_repr(decimal, 4)
    for i in range(4):
        led = leds[i]
        digit = int(binaire[i])
        if digit == 1:
            led.on()
        else:
            led.off()