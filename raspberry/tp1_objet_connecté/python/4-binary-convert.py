from gpiozero import LED
from numpy import binary_repr

leds = [LED(17), LED(27), LED(22), LED(10)]

def display(decimal):
    binaire = binary_repr(decimal, 4)
    for i in range(4):
        led = leds[i]
        digit = int(binaire[i])
        # completer la fonction


# tester la fonction pour des entiers differents
display(7)