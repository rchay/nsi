from flask import Flask, request

app = Flask(__name__)

@app.route("/getrequest/")
def getrequest():
    param1 = request.args.get('param1')         # obtention du parametre param1
    return "Le parametre est " + param1         # de la requete

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
