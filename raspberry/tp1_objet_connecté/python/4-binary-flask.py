from flask import Flask, request
from gpiozero import LED
from numpy import binary_repr

app = Flask(__name__)
leds = [LED(17), LED(27), LED(22), LED(10)]

def display(decimal):
    binaire = binary_repr(decimal, 4)
    for i in range(4):
        led = leds[i]
        digit = int(binaire[i])
        if digit == 1:
            led.on()
        else:
            led.off()

@app.route("/binaire/")
def binaire():
    decimal = int(request.args.get('decimal'))
    ...                                             #Completer la fonction
    return "Affichage binaire de " + str(decimal)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
