@app.route("/on/")
def on():
    led.on()
    return "Lampe allumee - <a href='/off/'>Eteindre la lampe</a>"

@app.route("/off/")
def off():
    led.off()
    return "Lampe eteinte - <a href='/on/'>Allumer la lampe</a>"