>>> from numpy import binary_repr
>>> binary_repr(0, 4)
'0000'
>>> binary_repr(1, 4)
'0001'
>>> binary_repr(7, 4)
'0111'
>>> binary_repr(15, 4)
'1111'