from flask import Flask
from gpiozero import LED

led = LED(17)
app = Flask(__name__)

@app.route("/on/")
def on():
    led.on()
    return "Lampe allumee"

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
