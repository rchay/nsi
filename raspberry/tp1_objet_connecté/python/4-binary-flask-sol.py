@app.route("/dels/")
def dels():
    decimal = int(request.args.get('decimal'))
    if decimal >=0 and decimal <=15:
        display(decimal)
        return "Affichage binaire de " + str(decimal)
    else:
        return "Le nombre doit etre compris entre 0 et 15"

